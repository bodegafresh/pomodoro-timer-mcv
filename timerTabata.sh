#!/bin/bash
##
# Pequeño script para ejecutar la app pomodoro
# Author: Marco Cerda Veas
##

electron --enable-transparent-visuals --disable-gpu --no-sandbox src/app.js &> /dev/null&