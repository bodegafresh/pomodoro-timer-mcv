#!/bin/sh

REAL_VERSION="0.0.0"
TEMP_FILE=tmpFile.md
CHANGELOG_FILE=changeLog.md

complete_file()
{
  countLine=0
  while IFS= read line
  do
    if [ $countLine -ne 0 ]; then
      echo "$line" >> $TEMP_FILE
    fi
    countLine=$(($countLine + 1))
  done <"$CHANGELOG_FILE"

  mv $TEMP_FILE $CHANGELOG_FILE
  rm -rf $TEMP_FILE
  # git add $CHANGELOG_FILE
  # git commit -m "doc: update changelog file" -n
}

update_file()
{
  branch_name=$(git rev-parse --abbrev-ref HEAD)

  complete_file
}

create_template()
{
  lineFirst=$(head -n 1 "$CHANGELOG_FILE")
  echo "$lineFirst" >> $TEMP_FILE
  DATEE=$(TZ=America/Santiago date +"%Y/%m/%d")
  ARTIFACT="rubik-dmfo-pomodoro"
  text="> ### $ARTIFACT-$REAL_VERSION-SNAPSHOT - $DATEE\n>\n> #### **Created**\n> - N/A\n>\n> #### **Modified**\n> - N/A\n>\n> #### **Deleted**\n> - N/A\n>\n> #### **Description**\n> - N/A\n>\n***"
  IFS=$'\n'
  read -ra newarr <<< "$text"
  for val in "${newarr[@]}";
  do
    echo "$val" >> $TEMP_FILE
  done
  update_file
}


update_changelog()
{
  fileChangeLog=$(cat "$CHANGELOG_FILE")
  lastChangelogVersion=$(echo "$fileChangeLog" | grep '> ### ' | sed -n '1p' | grep -Eo '[0-9]\.[0-9]\.[0-9]+')
  lastGitVersion=$(git describe --tags --abbrev=0)
  if [ "$lastChangelogVersion" == "$lastGitVersion" ]; then
    IFS='.'
    read -ra splitVersion <<< "$lastGitVersion"
    lastValue=${splitVersion[2]}
    lastValue="$(($lastValue + 1))"
    REAL_VERSION="${splitVersion[0]}.${splitVersion[1]}.$lastValue"
    create_template
  else
    echo "Strings are not equal"
  fi
}

update_changelog
