const workTime = 1200000;
const restTime = 300000;
const cicloTime = 1000;
const audioRegresivo = new Audio('../../resource/sounds/sound03.mp3');
const audioinicio = new Audio('../../resource/sounds/sound05.mp3');
const audioFin = new Audio('../../resource/sounds/sound04.mp3');
const ipc = require('electron').ipcRenderer;

let contadorTiempo = 0;
let trabajo, descanso;
let isReproducido = false;

function tiempoTrabajo() {
    let distance = workTime - contadorTiempo;
    contadorTiempo = contadorTiempo + cicloTime;

    if (contadorTiempo == cicloTime) {
        audioinicio.play();
        document.getElementById("txtMomento").innerHTML = "Trabajo";
        document.getElementById("txtMomento").style.color = 'blue';
        document.getElementById("txtTiempo").style.color = 'blue';
    }
    document.getElementById("txtTiempo").innerHTML = getTime(distance);
    if (distance <= 0) {
        clearInterval(trabajo);
        audioFin.play();
        initRest();
    }
}

function tiempoDescanso() {
    let distance = restTime - contadorTiempo;
    contadorTiempo = contadorTiempo + cicloTime;
    if (distance == 5000 && !isReproducido) {
        audioRegresivo.play();
        isReproducido = true;
    }
    if (contadorTiempo == cicloTime){
        document.getElementById("txtMomento").innerHTML = "Descanso";
        document.getElementById("txtMomento").style.color = 'green';
        document.getElementById("txtTiempo").style.color = 'green';
    }

    document.getElementById("txtTiempo").innerHTML = getTime(distance);
    if (distance <= 0) {

        clearInterval(descanso);
        isReproducido = false;
        initWork();
    }
}

async function initWork() {
  ipc.send('work-event', 'work');
  let myNotification = new Notification("Pomodoro", {
        body: "Iniciando ciclo de trabjo"
      });
    contadorTiempo = 0;
    trabajo = await setInterval(tiempoTrabajo, 1000);
}

async function initRest() {
  ipc.send('work-event', 'rest');
  let myNotification = new Notification("Pomodoro", {
        body: "A descansar!!!"
      });
    contadorTiempo = 0;
    descanso = await setInterval(tiempoDescanso, 1000);
}


function getTime(distance) {
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let formattedMinutes = ("0" + minutes).slice(-2);
    let formattedSeconds = ("0" + seconds).slice(-2);

    return `${formattedMinutes} : ${formattedSeconds}`;
}



window.onload = function () {

    initWork();
}
