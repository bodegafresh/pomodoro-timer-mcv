const { app, BrowserWindow, ipcMain, Tray, screen, Menu } = require('electron')
const path = require('path')

const assetsDirectory = path.join(__dirname, '../resource/');
const restIcon = 'icon1.png';
const workIcon = 'icon2.png';

let tray = undefined
let window = undefined

app.on('ready', () => {

    createTray()
    createWindow()
})

// Quit the app when the window is closed
app.on('window-all-closed', () => {
    app.quit()
})

// Creates tray image & toggles window on click
const createTray = () => {
    tray = new Tray(path.join(assetsDirectory, restIcon));
    tray.setTitle('Pomodoro');
    tray.setContextMenu(createMenu());
    tray.on('click', function (event) {
        console.log(event);
        toggleWindow()
    })
}

const createMenu = () => {
    let menu = Menu.buildFromTemplate([
        {
            label: 'Pomodoro',
            click: toggleWindow
        },
        {
            label: 'Salir',
            click: function () {
                app.quit();
            }
        }
    ]);
    return menu;
}

const getWindowPosition = () => {
    const trayBounds = tray.getBounds()
    const { width, height } = screen.getPrimaryDisplay().workAreaSize

    const x = trayBounds.x + (width - 370);
    const y = trayBounds.y + (height - 80);

    return { x: x, y: y }
}

// Creates window & specifies its values
const createWindow = () => {
    window = new BrowserWindow({
        width: 370,
        height: 80,
        show: false,
        fullscreenable: false,
        resizable: false,
        transparent: true,
        frame: false,
        webPreferences:{nodeIntegration:true}
    })
    window.loadURL('file://' + __dirname + '/html/pomodoro.html');
    window.on('blur', () => {
        if (!window.webContents.isDevToolsOpened()) {
            window.hide()
        }
    })
}

const toggleWindow = () => {
    if (window.isVisible()) {
        window.hide()
    } else {
        showWindow()
    }
}

const showWindow = () => {
    const position = getWindowPosition()
    window.setPosition(position.x, position.y, false)
    window.show()
    window.focus()
}
 ipcMain.on('work-event', function(event, title){   
   if(title==='work'){
     tray.setImage(path.join(assetsDirectory, workIcon));

   }else {
     tray.setImage(path.join(assetsDirectory, restIcon));
   }
 });


ipcMain.on('show-window', () => {
    showWindow()
})
